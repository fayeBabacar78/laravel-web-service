<?php

use Illuminate\Support\Facades\Route;

Route::prefix('auth')->group(function () {
    Route::middleware('guest')->group(function () {
        Route::post('register', 'Auth\RegisterController@register');
        Route::post('login', 'Auth\LoginController@login');
    });

     Route::middleware('auth:api')->group(function () {
     Route::get('logout', 'Auth\LogoutController');
     Route::get('user', 'Auth\UserController');
 });

});
