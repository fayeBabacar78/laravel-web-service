<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class LogoutController extends Controller
{
    /**
     * @return Response
     */
    public function __invoke()
    {
        Auth::user()->token()->revoke();

        return response()->noContent();
    }
}
