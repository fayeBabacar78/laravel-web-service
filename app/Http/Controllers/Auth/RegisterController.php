<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $validator = validator($request->all(), $this->rules());

        if ($validator->fails()) return $validator->errors()->all();

        $inputs = $this->secure_password($validator->validated());

        return User::create($inputs);
    }

    private function secure_password(array $validated)
    {
        $validated['password'] = Hash::make($validated['password']);

        return $validated;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    private function rules()
    {
        return [
            'prenom' => 'required',
            'nom' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed'
        ];
    }
}
