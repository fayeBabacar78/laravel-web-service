<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class LoginController extends Controller
{
    private $rules = ['password' => 'required', 'email' => 'required'];

    /**
     * @param Request $request
     * @return ResponseFactory|Response
     */
    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        $validator = validator($credentials, $this->rules);

        if ($validator->fails()) {
            return response($validator->errors()->all(), 401);
        }

        if (Auth::attempt($credentials, $request->remember_me)) {
            return $this->generate_token(Auth::user());
        }

        throw new BadRequestHttpException("L'authentification a échoué. Veuillez vérifier vos identifiants");
    }

    /**
     * @param User $user
     * @return ResponseFactory|Response
     */
    private function generate_token(User $user)
    {
        $result = $user->createToken('Access Token');

        $tokenInstance = $result->token; # Token model instance

        if (request()->remember_me) {
            $tokenInstance->expires_at = Carbon::now()->addWeek();
        }

        $tokenInstance->save();

        $token = [
            'access_token' => $result->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => date_format($result->token->expires_at, "Y-m-d\TH:i:s\Z")
        ];

        return response($token);
    }
}
